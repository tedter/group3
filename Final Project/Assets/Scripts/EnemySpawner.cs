﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {
	
	[SerializeField] private GameObject greenTriangle;
    [SerializeField] private GameObject greenSquare;
    [SerializeField] private GameObject yellowSquare;  
    [SerializeField] private Vector3[] waveVec;

	[SerializeField] public Text score;
	[SerializeField] public Text remain;

	private List<GameObject> currentWave = new List<GameObject>();
	private int waveNum;
    private bool check;
    private float startTime;
    private float currentTime;
    
	// Use this for initialization
	void Start () {
		//startTime = Time.time;
		waveNum= 1;
        //Wait 5 seconds then start the first wave

		wave(2,2,2);
	}
	
	// Update is called once per frame
	void Update () {
		//keep current time up to date
		currentTime = Time.time;

        //Loop through current wave and remove killed units
        for (int i = currentWave.Count - 1; i >= 0; i--)
        {
            Health current = currentWave[i].GetComponent<Health>();
            if (current.CurrentHealth == 0)
            {
                currentWave.Remove(currentWave[i]);
            }
        }

        if (currentWave.Count==0){
            //Stope after 2 waves
//            if (waveNum > 1)
//            {
//                return;
//            }
			waveNum++;
			wave(waveNum+2,waveNum+2,waveNum+2);
		}
	}

	void OnGUI () {
		string currWave = "Current Wave: " + waveNum;
		score.text = currWave;
		remain.text = "Enemies Remaining: " + currentWave.Count;
	}

	void wave(int t1, int t2, int t3){

		for(int i=0;i<t1;i++){
			Vector3 randvec = new Vector3(Random.Range(-5, 6), 0, 0);
			GameObject temp = Instantiate(greenTriangle) as GameObject;
			currentWave.Add(temp);
			temp.transform.position = waveVec[(i%2)]+randvec;
		}
        StartCoroutine(part2(t2));
        StartCoroutine(part3(t3));


    }
    IEnumerator part2(int t2)
    {
        yield return new WaitForSeconds(1);
        for (int i = 0; i < t2; i++)
        {
            Vector3 randvec = new Vector3(Random.Range(-6, 6), 0, 0);
            GameObject temp = Instantiate(greenSquare) as GameObject;
            currentWave.Add(temp);
            temp.transform.position = waveVec[(i % 2) + 2] + randvec;
        }
    }
    IEnumerator part3( int t3)
    {
        yield return new WaitForSeconds(2);
        for (int i = 0; i < t3; i++)
        {
            Vector3 randvec = new Vector3(Random.Range(-5, 6), 0, 0);
            GameObject temp = Instantiate(yellowSquare) as GameObject;
            currentWave.Add(temp);
            temp.transform.position = waveVec[(i % 2) + 4] + randvec;
        }
    }




}
