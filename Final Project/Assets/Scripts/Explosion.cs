﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{

	public class Explosion : MonoBehaviour
	{

		public float time = 0.2f;
		public int damage = 10;
		protected Health _colliderHealth;
		private float start;
		// Use this for initialization
		void Start ()
		{
			start = Time.time;
		}
	
		// Update is called once per frame
		void Update ()
		{
			if (Time.time - start > time) {
				Kill ();
			}
		}

		void Kill ()
		{
			Destroy (this.gameObject);
		}

		void OnTriggerStay2D (Collider2D collider)
		{
			_colliderHealth = collider.GetComponent<Health> ();
			_colliderHealth.Damage(damage, gameObject,0f,0f);
		}
		void OnTriggerEnter2D(Collider2D collider){
			_colliderHealth = collider.GetComponent<Health> ();
			_colliderHealth.Damage (damage, gameObject, 0f, 0f);
		}
	}
}
