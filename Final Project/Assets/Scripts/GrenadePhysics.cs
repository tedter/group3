﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
	public class GrenadePhysics : MonoBehaviour
	{
		public Grenade grenade;
		[SerializeField] private GameObject bomb;
		public float _detonateTime = 5;
		private bool firstCollide = true;
		private float _startTime;
		public float deadDrop = 0.0f;
		protected Health _health;
		// Use this for initialization
		void Awake ()
		{
			
			_health = GetComponent<Health>();
		}

		void OnEnable(){
			_startTime = Time.time;
		}
	
		// Update is called once per frame
		void Update ()
		{
			if (Time.time - _startTime > _detonateTime) {
				if (_health != null)
				{
					GameObject explosion = Instantiate (bomb) as GameObject;
					explosion.transform.position = this.gameObject.transform.position;
					_health.Damage(10,gameObject,.1f,0f);
				}	
			}
		}
			
			

	}
}